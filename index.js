var request = require('request');
var Service, Characteristic;

module.exports = function( homebridge ) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory( "homebridge-noolite-temperature", "NooLite-temperature", TemperatureAccessory );
};

function TemperatureAccessory( log, config ) {
  this.log = log;

  // Config
  this.config = config;
  this.name = config.name;
  this.nooliteHub = config.nooliteHub;
  this.channel = config.channel;

  this.battery = null;

  this.log( "Initialized '" + this.name + "'" );
}


TemperatureAccessory.prototype = {
    getServices: function() {

        var informationService = new Service.AccessoryInformation();
        informationService
            .setCharacteristic(Characteristic.Manufacturer, 'NooLite')
            .setCharacteristic(Characteristic.Model, 'PT111/PT112')
            .setCharacteristic(Characteristic.SerialNumber, '0.1');

        var temperatureService = new Service.TemperatureSensor(this.name);
        temperatureService
            .getCharacteristic(Characteristic.CurrentTemperature)
            .on('get', this.getState.bind(this));
        temperatureService
            .addCharacteristic(new Characteristic.StatusLowBattery())
            .on('get', this.getLowBatteryStatus.bind(this));

        return [ informationService, temperatureService ];
    },

    getLowBatteryStatus: function(callback) {
        var that = this;
        that.log("getting BatteryStatus for " + that.config.name);

        if (isNaN(this.battery) || this.battery == 1){
            callback(null, Characteristic.StatusLowBattery.BATTERY_LEVEL_LOW);
        } else {
            callback(null, Characteristic.StatusLowBattery.BATTERY_LEVEL_NORMAL);
        }
    },

    getState: function (callback) {

        var url = 'http://' + this.nooliteHub + '/sens?type=temp&ch=' + this.channel;

        this.log('Requesting temperature on "' + url);
        request({url: url, method: 'GET'}, function (error, res, body) {

            var sensor_data = null;
            var temperature = null;

            if (error) {
                this.log('HTTP bad response (' + url + '): ' + error.message);
            } else {
                try {
                    sensor_data = JSON.parse(body);
                    this.battery = sensor_data.battery;
                    temperature = sensor_data.temperature;
                    this.log('HTTP successful response: ' + body);
                } catch (parseErr) {
                    this.log('Error processing received information: ' + parseErr.message);
                    error = parseErr;
                }
            }
            callback(error, temperature);
        }.bind(this));
    }
};
